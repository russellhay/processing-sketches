## Processing Sketches

This is a repo of all of my processing sketches.  Because I believe in
sharing and open access to code, I'm making these available.

The directory layout is as follows:

   * *experiments* - small experiments that are used to understand concepts
   * *installations* - installations that are or have been on display in a gallery
   * *printworks* - systems used to generate print work, most of the time they output pdfs
   * *misc* - anything that doesn't quite fit.

## Current Release

### v1.0

## Change List

* trapeze1 - released
