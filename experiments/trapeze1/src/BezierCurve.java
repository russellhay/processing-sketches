import processing.core.*;
import processing.data.FloatHash;
import seltar.motion.*;

/**
 * Created with IntelliJ IDEA.
 * User: rhay
 * Date: 3/1/13
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class BezierCurve {
    private PApplet parent;
    private Motion a, b, c1, c2;
    private float last_x, last_y;
    private int strokeColor;
    private boolean is_done = false;
    private int stable_count = 0;
    private float stroke_weight;
    private float alpha;
    private final int STABLE_FRAME_COUNT = 35;
    private final int STABLE_RANGE = 10;

    public BezierCurve(PApplet parent, int stroker) {
        this.parent = parent;

        randomize(stroker);
    }

    private Motion randomPoint() {
        Motion retval = new Motion(parent.random(parent.width),
                parent.random(parent.height));
        retval.setDamping(0.98f);

        return retval;
    }

    public void draw(int renderMode) {
        alpha = 255 - (250 - renderMode * 200);
        // Go from 0 to 360
        parent.pushStyle();
        parent.stroke(strokeColor, alpha);
        parent.noFill();
        parent.strokeWeight(stroke_weight);
        parent.bezier(a.getX(), a.getY(),
                      c1.getX(), c1.getY(),
                      c2.getX(), c2.getY(),
                      b.getX(), b.getY());
//        for(double t=0; t< Math.PI*2; t+=0.1) {
//            double st = (1.0 + Math.cos(t))/2.0;
//            drawTimeSlice((float)st);
//        }
        parent.popStyle();

        if (renderMode > 0) {
            parent.pushStyle();
            parent.noStroke();

            parent.fill(255, 0, 0);
            parent.ellipse(a.getX(), a.getY(), 10, 10);
            parent.ellipse(b.getX(), b.getY(), 10, 10);

            parent.fill(255, 0, 255);
            parent.ellipse(c1.getX(), c1.getY(), 10, 10);
            parent.ellipse(c2.getX(), c2.getY(), 10, 10);
            parent.popStyle();
        }
    }

    public void update() {
        a.springTo(parent.width/2.0f+parent.noise(b.getX())*10.0f,
                   parent.height/2.0f+parent.noise(b.getY())*10.0f);
        a.wrap(0,0,parent.width, parent.height);

        b.springTo(parent.width / 2.0f + parent.noise(a.getX()) * 10.0f,
                parent.height / 2.0f + parent.noise(a.getY()) * 10.0f);
        b.wrap(0,0,parent.width, parent.height);

        a.move();
        b.move();

        c1.springTo(c2.getX(), c2.getY());
        c2.springTo(c1.getX(), c1.getY());
        c1.move();
        c2.move();

        if (a.getDistance() < STABLE_RANGE && b.getDistance() < STABLE_RANGE) {
            stable_count++;
            if(stable_count > STABLE_FRAME_COUNT) {
                is_done = true;
            }
        } else {
            stable_count = 0;
        }

        stroke_weight = a.getDistance()+1;
    }

    public boolean done() {
        return is_done;
    }

    public float getWeight() {
        return stroke_weight;
    }

    public int getColor() {
        return strokeColor;
    }

    public void draw() {
        draw(0);
    }

    private void drawTimeSlice(float st) {
        float x, y;
        x = (calculate_x(st));
        y = (calculate_y(st));
//        x += parent.noise(x)*10.0f-5.0f;
//        y += parent.noise(y)*10.0f-5.0f;

        if (last_x >= 0 && last_y >= 0) {
            parent.line(x, y, last_x, last_y);
        }
        last_x = x;
        last_y = y;
    }

    private float calculate_y(float st) {
        return calculate(st, a.getY(), c1.getY(), c2.getY(), b.getY());
    }

    private float calculate(float st, float p1, float p2, float p3, float p4) {
        float factor1 = (float)(Math.pow(1.0-st, 3)*p1);
        float factor2 = (float)(Math.pow(1.0-st, 2)*3*st*p2);
        float factor3 = (float)(Math.pow(st, 2)*3.0*(1.0-st)*p3);
        float factor4 = (float)(Math.pow(st, 3)*p4);

        return factor1 + factor2 + factor3 + factor4;
    }

    private float calculate_x(float st) {
        return calculate(st, a.getX(), c1.getX(), c2.getX(), b.getX());
    }

    public void randomize(int color) {
        this.a = randomPoint();
        this.b = randomPoint();
        this.c1 = randomPoint();
        this.c2 = randomPoint();

        this.strokeColor = color;
        this.is_done = false;
        this.stable_count = 0;

        this.last_x = -1.0f;
        this.last_y = -1.0f;
    }

    public float getAlpha() {
        return alpha;
    }

    public PVector getA() {
        return new PVector(a.getX(), a.getY());
    }

    public PVector getC1() {
        return new PVector(c1.getX(), c1.getY());
    }

    public PVector getC2() {
        return new PVector(c2.getX(), c2.getY());
    }

    public PVector getB() {
        return new PVector(b.getX(), b.getY());
    }
}
