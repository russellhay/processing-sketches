import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PVector;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: rhay
 * Date: 3/3/13
 * Time: 2:01 PM
 */
public class HistoryPoint {
    private PVector[] points;
    private int color;
    private float width;
    private float alpha;
    private PApplet parent;

    public HistoryPoint(PApplet parent, int color, float alpha, float width, PVector a, PVector c1, PVector c2, PVector b) {
        this.points = new PVector[4];
        this.color = color;
        this.width = width;
        this.alpha = alpha;
        this.parent = parent;

        points[0] = a;
        points[1] = c1;
        points[2] = c2;
        points[3] = b;
    }

    public void draw(PGraphics g)
    {
        g.pushStyle();
        g.strokeWeight(width);
        g.stroke(color, alpha);
        g.bezier(
                points[0].x, points[0].y,
                points[1].x, points[1].y,
                points[2].x, points[2].y,
                points[3].x, points[3].y
        );
        g.popStyle();
    }

    public void draw()
    {
        parent.pushStyle();
        parent.strokeWeight(width);
        parent.stroke(color, alpha);
        parent.bezier(
                points[0].x, points[0].y,
                points[1].x, points[1].y,
                points[2].x, points[2].y,
                points[3].x, points[3].y
        );
        parent.popStyle();
    }
}
