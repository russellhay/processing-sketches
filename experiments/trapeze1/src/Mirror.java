import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * Created with IntelliJ IDEA.
 * User: rhay
 * Date: 3/2/13
 * Time: 8:36 PM
 */
public class Mirror {
    private final PApplet parent;
    private final Drawable render;
    private final PGraphics gfx;
    private final PGraphics mask;
    private final float angleDegree = 30;
    private final float angle;

    public Mirror(PApplet parent, Drawable render) {
        this.parent = parent;
        this.render = render;
        this.gfx = parent.createGraphics(parent.width, parent.height, parent.P2D);
        this.mask = parent.createGraphics(parent.width, parent.height, parent.P2D);

        angle = (float)Math.toRadians(angleDegree);

        mask.beginDraw();
        mask.fill(255);
        mask.triangle(0.0f,0.0f,
                0.0f, (float)mask.height/2.0f,
                (float)(-mask.height/2.0f*Math.sin(angle)),
                (float)(mask.height/2.0f*Math.cos(angle)));
//        mask.fill(255,0 ,0);
//        mask.ellipse(0.0f, (float)mask.height/2.0f, 10, 10);
//        mask.fill(0,255,0);
//        mask.ellipse((float)(-mask.height/2.0f*Math.sin(angle)),
//                (float)(mask.height/2.0f*Math.cos(angle)), 10, 10);
        mask.endDraw();

        gfx.beginDraw();
        gfx.background(255);
        gfx.endDraw();
    }

    public void draw() {
        gfx.beginDraw();
        gfx.pushMatrix();
        // gfx.translate(-gfx.width/4.0f, -gfx.height/4.0f);
        render.render(gfx);
        gfx.popMatrix();
        gfx.mask(mask);
        gfx.endDraw();

        parent.background(255);
        parent.pushMatrix();
        parent.translate(parent.width/2.0f, parent.height/2.0f);
        // parent.image(mask, 0, 0);
        for(int i=0;i<360;i+= angleDegree) {
            parent.rotate((float)Math.toRadians(i));
            parent.image(gfx, 0, 0);
        }
        parent.popMatrix();
    }
}
