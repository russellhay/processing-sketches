import processing.core.*;
import processing.pdf.*;

/**
 * Created with IntelliJ IDEA.
 * User: rhay
 * Date: 3/1/13
 * Time: 8:07 PM
 */
public class PDFRecorder {
    private final PApplet parent;
    private PGraphicsPDF canvas;
    private boolean recording, quit_after_x;
    private int output_count = 0;
    private final int QUIT_AFTER_X = 100;

    public PDFRecorder(PApplet parent) {
        this.parent = parent;
        this.canvas = createGraphics();
        this.recording = false;
        this.quit_after_x = false;
    }

    private PGraphicsPDF createGraphics() {
        return (PGraphicsPDF)parent.createGraphics(parent.width, parent.height,
                PGraphicsPDF.PDF, getFilename("pdf"));
    }

    public void keyPressed(char key) {
        if (key != 'r' && key != 'q' && key != 'g') return;
        if (key == 'q') {
            finishRecording();
            parent.exit();
        } else if (key == 'r' && recording) {
            finishRecording();
        } else if (key == 'r') {
            startRecording();
        } else if (key == 'g') {
            quit_after_x = true;
            startRecording();
        }
    }

    public void startRecording() {
        if(!recording) {
            parent.println("Recording");
            parent.beginRecord(canvas);
            recording = true;
        }
    }

    public void finishRecording(boolean save_start_new) {
        if(recording) {
            parent.println("Done.");
            parent.endRecord();
            recording = false;

            parent.saveFrame(getFilename("jpg"));

            if(save_start_new) {
                output_count++;
                if(quit_after_x && output_count > QUIT_AFTER_X) {
                    parent.exit();
                }
                canvas = createGraphics();
                startRecording();
            }

        }

    }

    public void finishRecording() {
        finishRecording(false);
    }

    public String getFilename(String extension) {
        return parent.sketchPath("output" + output_count + "." + extension);
    }
}
