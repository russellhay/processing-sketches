import processing.core.PGraphics;

/**
 * Created with IntelliJ IDEA.
 * User: rhay
 * Date: 3/2/13
 * Time: 8:37 PM
 */
public interface Drawable {
    void render(PGraphics g);
}
