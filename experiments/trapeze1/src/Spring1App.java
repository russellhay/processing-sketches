/**
 * Created with IntelliJ IDEA.
 * User: rhay
 * Date: 2/28/13
 * Time: 1:10 PM
 *
 * Key Commands:
 *  r - toggle recording
 *  c - change color
 *  q - finish recording and quit
 *  g - record 100 cycles and then quit
 */

import colorLib.Palette;
import colorLib.RandomPalette;
import colorLib.Swatch;
import processing.core.*;
import seltar.motion.*;
import sun.font.TrueTypeFont;

public class Spring1App extends PApplet {
    private PDFRecorder recorder;
    private BezierCurve[] curves;
    private Palette colors;
    private boolean clear = false;
    private int renderMode = 0;
    private BezierHistoryTracker history;
    private int angleDegree;

    private final String swatchFile = "Pumpkin.cs"; // "COLOURlovers.com_Shoes_kick_Asses.cs"

    public void setup() {
        size(500, 500, P2D);
        smooth();
        frameRate(30);
        background(255);

        sketchPath = "/Users/rhay/code/processing/sketches/experiments/trapeze1/data";
        colors = new Palette(this);
        colors.makeComplementary(color(random(255), random(255), random(255)));
//        colors.deleteDuplicate();
//        colors.addColor(color(248, 238, 223));
//        colors.addColor(color(206,0,78));
//        colors.addColor(color(56,39,9));
//        colors.addColor(color(88,69,34));
//        colors.addColor(color(162,137,96));
        println("Total Colors: " + colors.totalSwatches());
        curves = new BezierCurve[colors.totalSwatches()*2];
        for(int i=0; i<curves.length; i++) {
            curves[i] = new BezierCurve(this, randomColor());
        }
        recorder = new PDFRecorder(this);
        history = new BezierHistoryTracker(this);
    }

    private int randomColor() {
        return colors.getSwatch((int)random(colors.totalSwatches())).getColor();
    }

    private void randomizePalette() {
        colors = new Palette(this);
        colors.makeComplementary(color(random(255), random(255), random(255)));
    }

    public void mouseClicked() {
        clear = true;
    }

    public void keyPressed() {
        if (key == ' ') {
            renderMode = (renderMode + 1) % 2;
            recorder.finishRecording();
            background(255);
        } else if (key == 'c') {
            randomizePalette();
            clear = true;
        } else {
            recorder.keyPressed(key);
        }
    }

    public void draw() {
        pushMatrix();
        translate(width/2, height/2);
        rotate(radians(angleDegree));
        translate(-width/2, -height/2);
        render();
        popMatrix();

        angleDegree = (int)noise(angleDegree + 1.0f) % 360;
    }

    public void render() {
        if(renderMode > 0) {
            background(255);
        }
        if (clear) {
            recorder.finishRecording(true);
            background(255);
            for(int i=0; i<curves.length; i++) {
                curves[i].randomize(randomColor());
            }
            clear = false;
        }

        clear = true;
        for(int i=0; i<curves.length; i++) {
            curves[i].update();
            curves[i].draw(renderMode);
            clear = clear && curves[i].done();
        }
    }

//    public void render(PGraphics g) {
//        // This duplicate code is unavoidable
//
//        if(renderMode > 0) {
//            g.background(255);
//        }
//
//        if (clear) {
//            g.background(255);
//            for(int i=0; i<curves.length; i++) {
//                curves[i].randomize(randomColor());
//            }
//            clear = false;
//        }
//
//        for(int i=0; i<curves.length; i++) {
//            curves[i].update(g);
//            curves[i].draw(g, renderMode);
//        }
//    }
}
