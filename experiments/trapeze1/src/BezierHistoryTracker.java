import processing.core.PApplet;
import processing.core.PGraphics;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: rhay
 * Date: 3/3/13
 * Time: 1:58 PM
 */
public class BezierHistoryTracker {
    private final PApplet parent;
    private ArrayList<HistoryPoint> historyPoints;

    public BezierHistoryTracker(PApplet parent) {
        this.parent = parent;
        this.historyPoints = new ArrayList();
    }

    public void add(BezierCurve curve) {
        HistoryPoint point = new HistoryPoint(parent,
                curve.getColor(),
                curve.getAlpha(),
                curve.getWeight(),
                curve.getA(),
                curve.getC1(),
                curve.getC2(),
                curve.getB()
        );

        this.historyPoints.add(point);
    }

    public void clear() {
        this.historyPoints.clear();
    }

    public void draw(PGraphics g) {
        g.beginDraw();
        g.background(255);
        for(int i=0; i<historyPoints.size(); i++) {
            historyPoints.get(i).draw(g);
        }
        g.endDraw();
    }

    public void draw() {
        parent.background(255);
        for(int i=0; i<historyPoints.size(); i++) {
            historyPoints.get(i).draw();
        }
    }
}
